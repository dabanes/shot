import java.io.*;
import java.util.*;
import java.lang.*;
public class Abanes_FastFood{
			public static void main(String [] Arghs)throws IOException{

				Abanes_FastFoodCounter sam = new Abanes_FastFoodCounter();// Initiate a variable to use the functions in fastfood counter
				String[] line = readAfile();// get the all the data from the text file
				int totl = line.length;// get all the length of the array

					sam.beginOrder();//call the beginOrder
				find(sam,line);//do all the things
				return;

			}
			public static String[] readAfile()throws IOException{
				BufferedReader bufferedReader = new BufferedReader(new FileReader("orders.txt"));
			  StringBuilder sb = new StringBuilder();
			  String line = bufferedReader.readLine(); // the current line
				// while loop that will traverse all the txt
				//begin order starts here.
			  while(line != null){
			    sb.append(line);// will get per line and append it in sb
			    sb.append(" ");// will change the line separator to space
			    line = bufferedReader.readLine();
			  }
			  bufferedReader.close();
			  String all = sb.toString();// convert to StringBuilder to String
			  String datas = all.replace(' ', ',');// change white spacess to comma
			  String [] cat = datas.split(",");// will split the data by a comma
				return cat;
			}
			public static void find(Abanes_FastFoodCounter sam,String[] cat)throws IOException{
				int[] counter =  new int[5];// supposedly get the counters
				int i = 0,quantity = 0,num = 0;//initialization of variables
				double tend_Amt = 0,total = 0,tot = 0;//initialization of variables
				String line;//initialization of variables
				boolean str;//to know if to write a insufficient amount or not
				while(i < cat.length){
					// conditional statements which will get the number of each orders taken on each counter
					if(cat[i].equals("counter")){
						num = Integer.parseInt(cat[i+1].toString())-1;//kukunin yung number ng counter and i increment to count total orders
						i+=2;
						tot = 0;// initialize total amount tente
					}
					// conditional statements which will get the number of quantity and the total price
					if(cat[i].equals("hamburger")){
						quantity = Integer.parseInt(cat[i+1].toString());//get the quantity of orders
						line = cat[i].toString();// convert orders to string
						sam.add(quantity , line);// give the quantity and line to the add functions
						  total = sam.getTotalPrice();// recieving total price of  per  order
						tot += total;//increment all total price of all orders per counter
							i+=2;
					}
					// conditional statements which will get the number of quantity and the total price
					if(cat[i].equals("cheeseburger")){
						quantity = Integer.parseInt(cat[i+1].toString());// convert orders to string
						line = cat[i].toString();
						 sam.add(quantity , line);// give the quantity and line to the add functions
						 total = sam.getTotalPrice();// recieving total price of  per  order
						tot += total;
						i+=2;
					}
					if(cat[i].equals("fries")){
						quantity = Integer.parseInt(cat[i+1].toString());
						line = cat[i].toString();
						 sam.add(quantity , line);
						 total = sam.getTotalPrice();
						tot += total;
						i+=2;
					}
					if(cat[i].equals("smalldrink")){
						quantity = Integer.parseInt(cat[i+1].toString());
						line = cat[i].toString();
						 sam.add(quantity , line);
						 total = sam.getTotalPrice();
						tot += total;
						i+=2;
					}
					if(cat[i].equals("largedrink")){
						quantity = Integer.parseInt(cat[i+1].toString());
						line = cat[i].toString();
						sam.add(quantity , line);
						total = sam.getTotalPrice();
						tot += total;
						i+=2;
					}
					if(cat[i].equals("end")){
						tend_Amt = Double.parseDouble(cat[i+1].toString());// get the amount recieved from customers
															//System.out.println(tend_Amt - total);
						i+=2;
					}
					if((tend_Amt - tot)< 0){
						str = false;
						sam.cancelOrder(num);//call the function to increment the total cancelOrders
						fileWrite(num,tend_Amt,tot,str);//file write
					}
					else{
						str = true;
						fileWrite(num,tend_Amt,tot,str);
						sam.countCash(num,tot);//call the function to increment the total cash
						sam.countOrders(num);// increment the count orders
					}
				}
				for(int l = 0; l< 5; l++){
					fileWrite2(counter,sam,l);//write the summary of the orders and counters
				}
			}

			public static void fileWrite(int num,double tend_Amt, double tot, boolean str)throws IOException{
				File file = new File("counters.txt");//will create a new file

				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}

				// true = append file
				FileWriter fw = new FileWriter(file,true);
	    	//BufferedWriter writer give better performance
	    	BufferedWriter bw = new BufferedWriter(fw);
				if(str){
	    		bw.write("Counter, " + (num+1)+" Total Price: "+tot + ", Changed returned: P" + (tend_Amt - tot));
					bw.write("\n");
				}else{
					bw.write("Counter, " + (num+1)+" Total Price: "+tot + ", Amount tendered is insufficient");
					bw.write("\n");
				}

	    	//Closing BufferedWriter Stream
	    	bw.close();
			}
			public static void fileWrite2(int[] counter,Abanes_FastFoodCounter sam,int l)throws IOException{
				int [] totl=  sam.completeOrder();
				File file = new File("counters.txt");

				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}

				// true = append file
				FileWriter fw = new FileWriter(file,true);
	    	//BufferedWriter writer give better performance
	    	BufferedWriter bw = new BufferedWriter(fw);
				bw.write("\n Counter " +(l+1) +" \n" );
				bw.write("\t\t Total orders completed: " +totl[l] + " \n" );
				bw.write("\t\t Total orders completed: " + sam.cancel[l] +" \n" );
				bw.write("\t\t Total orders completed: " + sam.total_cash[l] +" \n" );

			//Closing BufferedWriter Stream
				bw.close();

			}
}
